
// pull colors via magnitude. Much cleaner than big if else
function getColor(d) {
  return d > 7 ? '#800026' :
         d > 6  ? '#BD0026' :
         d > 5  ? '#E31A1C' :
         d > 4  ? '#FC4E2A' :
         d > 3   ? '#FD8D3C' :
         d > 2   ? '#FEB24C' :
         d > 1   ? '#FED976' :
                    '#FFEDA0';
};

function createMap(earthquakePoints, mangnitude) {

  var lightmap =  L.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}", {
    attribution: "Map data &copy; <a href=\"https://www.openstreetmap.org/\">OpenStreetMap</a> contributors, <a href=\"https://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, Imagery © <a href=\"https://www.mapbox.com/\">Mapbox</a>",
    maxZoom: 18,
    id: "mapbox.streets",
    accessToken: API_KEY
  });


  // Create a baseMaps object to hold the lightmap layer
  var baseMaps = {
    "Light Map": lightmap
  };

  // Create an overlayMaps object to hold the earthquakePoints layer
  var overlayMaps = {
    "Earthquakes": earthquakePoints
  };

  // Create the map object with options
  var map = L.map("map", {
    center: [37.77668498313053,-122.43490219116211 ],
    zoom: 12,
    layers: [lightmap, earthquakePoints]
  });

/*Legend specific*/
var legend = L.control({ position: "bottomright" });

legend.onAdd = function(map) {
  var div = L.DomUtil.create("div", "legend");
  div.innerHTML += "<h4>Magnitudes</h4>";
  div.innerHTML += '<i style="background: #FFEDA0"></i><span>0 - 1</span><br>';
  div.innerHTML += '<i style="background: #FED976"></i><span>1 - 2</span><br>';
  div.innerHTML += '<i style="background: #FEB24C"></i><span>2 - 3</span><br>';
  div.innerHTML += '<i style="background: #FD8D3C"></i><span>3 - 4</span><br>';
  div.innerHTML += '<i style="background: #FC4E2A"></i><span>4 - 5</span><br>';
  div.innerHTML += '<i style="background: #E31A1C"></i><span>5 - 6</span><br>';
  div.innerHTML += '<i style="background: #BD0026"></i><span>6 - 7</span><br>';
  return div;
};

legend.addTo(map);

  // Create a layer control, pass in the baseMaps and overlayMaps. Add the layer control to the map
  L.control.layers(baseMaps, overlayMaps, {
    collapsed: false
  }).addTo(map);
}


function createCircles(response) {

    // Pull the features off of response.data
    var earthquakes = response.features;
    // Initialize an array to earthquake locations
   var earthquakeCoordinates = [];
   var magnitudes = [];
    // // Loop through the earthquake array
   for (var index = 0; index < earthquakes.length; index++) {
 
    //For each earthquake, create a circle and bind a popup with quake info
    var earthquakeCoordinate = L.circleMarker([earthquakes[index].geometry["coordinates"][1], 
    earthquakes[index].geometry["coordinates"][0]], {
      radius: earthquakes[index].properties["mag"] *5,
      color: getColor(earthquakes[index].properties["mag"]),
      fillOpacity: 0.8

    })
      .bindPopup("<h3>" + earthquakes[index].properties["title"] + "</h3>");
      magnitudes.push(earthquakes[index].properties["mag"]);
    
    // Add the circles to the earthquakeCoordinates array
      earthquakeCoordinates.push(earthquakeCoordinate);
     };
  
    // Create a layer group made from the earthquake array, pass it into the createMap function
    createMap(L.layerGroup(earthquakeCoordinates), magnitudes);
  }
  // Perform an API call to the USGS to get quake information. Call createCircles when complete
d3.json("https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.geojson", createCircles);